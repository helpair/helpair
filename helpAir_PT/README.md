# helpAir 

a ajuda do ar é uma instalação de respiração intrusiva de controlo dos volumes de custos.

Permite-lhe configurar o início da operação a partir de um único menu:
- Volume das marés (% de rota)
- frequência respiratória
- a parte não assumida em espiral
- fornecimento de pausas 

O projeto foi desenvolvido espontaneamente por um grupo de empregados de Ferrovial e Snra, apoiada por http://respiradores4all.com

A IC3 desenvolveu a versão industrial do presente documento.

## Versões
Existem atualmente 3 versões do projeto:

- **Versão 1.1**:manutenção, motor NMA23, motor de Arduino e mecanismo de almofada respiratória.Esta versão é a versão que foi implementada.Permite que 500 ml de volume de TPS sejam alcançados através de inspiração.Não é possível ajustar o volume em linha com o caudal de ar expirado ou alterar a configuração do sistema em tempo real.

- **Versão 1.2**:mecanismo de êmbolo, motor NEM23XL com parafuso, Arduino e Ampú.Esta versão foi concebida mas não executada.Deve ser possível ultrapassar 1 000 ml de volume de Tridal numa forma imparcial.Não é possível ajustar o volume em linha com o caudal de ar expirado ou alterar a configuração do sistema em tempo real.

- **Versão 2.0**:mecanismo de êmbolo, motor NEM23XL com parafuso, PLC, Siemens e Ampú.Esta versão foi concebida mas não executada.Deve permitir ultrapassar 1 000 ml de volume T a título de inspiração, ajustar o volume determinado por volume ao caudal de ar e alterar a configuração do sistema em tempo real.

## Estado
Pode ver as versões como um roteiro lógico para um dispositivo final.Em função das necessidades de cobertura do roteiro, pode ser alterado.

O seu desenvolvimento pela atual equipa tem sido descontinuado devido à situação atual da cadeia de abastecimento de componentes, à evolução da pandemia em Espanha e ao grau de progresso de outros protótipos.

Pode ser um ponto de partida interessante para o desenvolvimento de novos protótipos em locais onde a pandemia está menos avançada.

## Aviso
**Um aparelho de proteção respiratória é um dispositivo médico em que a vida de uma pessoa depende, uma anomalia pode causar danos graves ou mesmo a morte.**

**A documentação publicada deste projeto permite o desenvolvimento de um protótipo de investigação respiratória.**

**A função de protótipo do ar não deve, em caso algum, ser considerada e não deve ser utilizada com seres humanos.**

**Atualmente, o protótipo não foi validado pela Agência Espanhola de Medicamentos.**

## Documentação (v.1.1, v.1.2 e v.2.0)
- Apresentação do projeto (v.1.1)
- Helpf industrial design (v.2.0)

## Eletrónico (v.1.1)
- Lista de materiais
- Diagrama esquemático

## Código (v1.1)
- Código aplicado no controlador de Arduino

## 3D desenhos (v.1.1, v.1.2 e v.2.0)
- Versão de 1.1 dos arquivos do STL
- Versão de 1.2 dos arquivos do STL
- Versão de 2.0 dos arquivos do STL

## Imagens (v.1.1, v.1.2 e v.2.0)
- Versão 1.1 das imagens
- Versão 1.2 das imagens
- Versão 2.0 das imagens

## Equipa de projeto
- Adrián de la Iglesia Valls
- Jorge Vallejo Fuentes
- Alejandro Pérez Ayo
- Francisco Iglesias Sánchez
- Ignacio Losas
- Mariano de Diego
- David López Astorga
- Cristina Cruz Aparicio
- Javier Lázaro Gaspar
- Dr. Eduardo Calle
- Dr. José Antonio de Paz

# Licença
útil é um projeto desenvolvido no âmbito da licença GNU GPv3

