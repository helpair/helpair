# helpAir

helpAir este o instalație de respirație invazivă pentru controlul volumului scăzut al costurilor.

Vă permite să configurați începerea operațiunii dintr-un meniu unic:
- Volumul mareei (% pe rută)
- frecvența respirației
- proporțiile nespiralizate
- livrarea de pauze de cafea

Proiectul a fost elaborat în mod spontan de un grup de angajați ai Ferrovial și Snulară susținută de http: //respiradores4all.com

IC3 a elaborat versiunea industrială a acestui document.

## Versiuni
În momentul de față există 3 de versiuni:

- **Version 1.1**: întreținere, motor NMA23, motor Arduino și sac respirator. Această versiune este versiunea care a fost pusă în aplicare. Permite atingerea unui volum de 500 ml de material TPidal, ca sursă de inspirație. Nu este posibilă ajustarea volumului în funcție de debitul aerului expirat sau modificarea configurației sistemului în timp real.

- **Version 1.2**: mecanismul cu piston, motorul NEEM23XL cu șurub, Arduino și Ambú. Această versiune a fost concepută, dar nu a fost pusă în aplicare. Trebuie să fie posibil să se depășească în mod imparțial 1 000 ml din volumul TRidal. Nu este posibilă ajustarea volumului în funcție de debitul aerului expirat sau modificarea configurației sistemului în timp real.

- **Version 2.0**: mecanismul cu piston, motorul NEEM23XL cu șurub, PLC de Siemens și Ambú. Această versiune a fost concepută, dar nu a fost pusă în aplicare. Acesta ar trebui să facă posibilă depășirea a 1 000 ml de volum Tidal cu ajutorul unei surse de inspirație, să ajusteze volumul determinat de volum în funcție de debitul de aer și să modifice configurația sistemului în timp real.

## Stat
Puteți vedea versiunile ca foaie de parcurs logică către un dispozitiv final. În funcție de necesități, se poate modifica foaia de parcurs.

Dezvoltarea acestora de către echipa actuală a fost continuată datorită stadiului în care se află lanțul de aprovizionare al componentelor, evoluției pandemiei în Spania și gradului de progres a altor prototipuri.

Poate fi un punct de plecare interesant pentru dezvoltarea de noi prototipuri în locurile în care pandemia este mai puțin avansată.

## Avertizare
**O mască de gaze este un dispozitiv medical de care depinde viața unei persoane, o defecțiune poate duce la daune grave sau chiar la deces.**

**Documentația publicată în acest proiect permite dezvoltarea unui prototip de cercetare privind respirația pentru cercetare.**

**În niciun caz funcția de prototip de ajutor nu ar trebui să fie luată în considerare ca echipament medical funcțional și nu ar trebui să fie utilizată împreună cu oamenii.**

**În prezent, prototipul nu a fost validat de Agenția spaniolă pentru medicamente.**

## Documentație (v.1.1, v.1.2 și v.2.0)
- Prezentarea proiectului (v.1.1)
- Helpff industrial design (v.2.0)

## Electronică (v.1.1)
- Bill BOM (Bill of Materials)
- Diagramă schematică

## Cod (v1.1)
- Cod pus în aplicare la operatorul Arduino

## 3d desene (v.1.1, v.1.2 și v.2.0)
- Arhive STL version 1.1
- Arhive STL version 1.2
- Arhive STL version 2.0

## Imagini (v.1.1, v.1.2 și v.2.0)
- Imagini versiunea 1.1
- Imagini versiunea 1.2
- Imagini versiunea 2.0

## Echipa proiectului
- Adrián de la Iglesia Valls
- Jorge Vallejo Fuentes
- Alejandro Pérez Ayo
- Iglesias Sánchez
- Ignacio Losas
- Mariano de Diego
- David López Astorga
- Cristina Cruz Aparicio
- Javier Lázaro/Gaspar
- Dr. Eduardo Calle
- Dr. José Antonio de Paz

## Licență
helpAir este un proiect elaborat în cadrul licenței GULPv3
