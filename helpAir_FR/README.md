# helpAir 

helpAir est un projet de ventilateur invasif à faible coût basé sur le contrôle du volume de gaz inhalé.

À partir d'un seul menu, les paramètres suivants peuvent être réglés au démarrage de l'appareil :
- Volume courant (% du parcours - pas utile, ne veut pas dire grand chose)
- Rythme respiratoire Fréquence respiratoire
- Rapport inspiration-expiration Rapport temps inspiratoire/temps expiratoire
- Rapport de pause pause inspiratoire ou temps de pause

Le projet a été développé spontanément à l’initiative d’un groupe par un groupe d'employés de Ferrovial et Sngular avec le soutien de http://respiradores4all.com 

CI3 en a conçu la version industrielle

## Versions
Actuellement, il existe 3 versions de conception :

- **Version 1.1** : mécanisme à came, moteur NEMA23, Arduino et ballon de ventilation. Cette version a été testée. Elle permet d'atteindre 500 ml de volume courant sur la voie inspiratoire. Elle ne permet pas d'ajuster le volume d’air poussé administré en fonction du débit d'air expiré ou de modifier la configuration du système en temps réel.

- **Version 1.2** : mécanisme à piston, moteur NEMA23XL avec broche, Arduino et ballon de ventilation. Cette version a été conçue mais pas encore implémentée. Elle doit permettre de dépasser 1000 ml de volume courant dans la voie inspiratoire dans les voies aériennes. Elle ne permet pas d'ajuster le volume d’air poussé administré en fonction du débit d'air expiré ou de modifier la configuration du système en temps réel.

- **Version 2.0** : mécanisme à piston, moteur NEMA23XL avec broche, PLC Siemens et ballon de ventilation. Cette version a été conçue mais pas encore implémentée. Elle doit permettre de dépasser 1000 ml de volume courant sur la voie inspiratoire dans les voies aériennes, d’ajuster le volume d’air poussé en fonction du débit d'air expiré et de modifier la configuration du système en temps réel.

## État
Les versions peuvent être considérées comme un guide logique afin d’atteindre un dispositif final. Selon les besoins à couvrir, ce guide peut être modifié.
Son développement par l'équipe actuelle a été interrompu en raison de la situation de la chaîne d'approvisionnement des composants, de l'évolution de la pandémie en Espagne et du degré d'avancement des autres prototypes.
Cela peut être un point de départ intéressant pour le développement de nouveaux prototypes dans des endroits où la pandémie est moins avancée.

## Avertissement
**Un respirateur est un équipement médical dont dépend la vie d'une personne ; un dysfonctionnement peut entraîner des dommages importants, voire la mort.**

**La documentation publiée liée à ce projet permet le développement d'un prototype de respirateur de recherche.**

**Le prototype helpAir ne doit en aucun cas être considéré comme un équipement médical fonctionnel et ne doit pas être utilisé sur les humains.**

**Actuellement, le prototype n'a PAS été validé par l'Agence espagnole des médicaments et des dispositifs médicaux.**

## Documentation (v.1.1, v.1.2 et v.2.0)
- Présentation du projet (v.1.1)
- Design helpAir industrial (v.2.0)

## Électronique (v.1.1)
- BOM Bill of Materials
- Schéma des connexions

## Code (v1.1)
- Code implémenté dans le contrôleur Arduino

## Conceptions 3D (v.1.1, v.1.2 et v.2.0)
- Fichiers STL version 1.1
- Fichiers STL version 1.2
- Fichiers STL version 2.0
- Images (v.1.1, v.1.2 et v.2.0)
- Images version 1.1
- Images version 1.2
- Images version 2.0

## Équipe du projet
- Adrián de la Iglesia Valls
- Jorge Vallejo Fuentes
- Alejandro Pérez Ayo
- Francisco Iglesias Sánchez
- Ignacio Losas Dávila
- Mariano de Diego
- David López Astorga
- Cristina Cruz Aparicio
- Javier Lázaro Gaspar
- Dr. Eduardo Calle
- Dr. José Antonio de Paz

## Licence
helpAir est un projet développé sous licence GNU GLPv3.


