# helpAir 
 
**[AR helpAir](./helpAir_AR/README.md)** هو مشروع تهوية غازية منخفض التكلفة يعتمد على التحكم في حجم الغاز المستنشق

**[DE helpAir](./helpAir_DE/README.md)** ist eine kostengünstige Intrusionsanlage für das Einatmen von Mengen.

**[EN helpAir](./helpAir_EN/README.md)** is a low-cost invasive ventilator project that is  “based on volume controlled ventilation”.

**[ES helpAir](./helpAir_ES/README.md)** es un proyecto de respirador por control de volumen de bajo coste.

**[FR helpAir](./helpAir_FR/README.md)** est un projet de ventilateur invasif à faible coût basé sur le contrôle du volume de gaz inhalé.

**[HR helpAir](./helpAir_HR/README.md)** je projekt jeftinog invazivnog respiratora koji kontrolira volumen.

**[PL helpAir](./helpAir_PL/README.md)** to tanie urządzenie do inwazyjnego oddychania o niskiej cenie.

**[PT helpAir](./helpAir_PT/README.md)** a ajuda do ar é uma instalação de respiração intrusiva de controlo dos volumes de custos.

**[RO helpAir](./helpAir_RO/README.md)** este o instalație de respirație invazivă pentru controlul volumului scăzut al costurilor.

**[UR helpAir](./helpAir_UR/README.md)** ہیلپ ایر ایک کم لاگت والا ناگوار وینٹیلیٹر پروجیکٹ ہے جو "حجم کنٹرول والی وینٹیلیشن پر مبنی ہے
 




 
 