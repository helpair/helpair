/*
 * //////////////////////////////////////  HELPAIR  //////////////////////////////////////
 * Code for 1 Arduino Nano
 * Nema 23 Motor
 * Driver TB6600
 * 2 solenoid valves
 * LCD by I2C 20x4
 * Encoder with push-button
 * and a buzzer
 * 
 * Developed by:
 * Adrián de la Iglesia Valls and Francisco Iglesias Sánchez
 * 
 * For more info, please visit: https://www.respiradores4all.com/prototipo/helpair
 * GNU GENERAL PUBLIC LICENSE V3
*/




#include <AccelStepper.h> //Library for the driver motor
#include <Wire.h> // Library of communication for I2C
#include <LiquidCrystal_I2C.h> // Library for LCD


const int stepsPerRevolution = 200; //Steps of the motor in one round
const int microsteps = 32; // Microsteps settings on driver


// Pins and settings of the driver for the motor
#define dirPin 9
#define stepPin 10
#define motorInterfaceType 1


//Pins for the encoder with push-button
#define DT 3
#define CLK 2
#define SW 4 //button click


#define buzzerPin 12     // Pin for buzzer
#define valveEspPin 7   // Pin for exhalation valve
#define valveInsPin 7  // Pin for inhalation valve



AccelStepper stepper = AccelStepper(motorInterfaceType, stepPin, dirPin);  // Definition of LCD with library
LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 20, 4);                   // Definition of LCD with library


// Medical parameters and its default values
int rpm = 15;                                // Breaths per minute
float recorrido = 0.50;                     // Percentage of distance travelled (1 corresponds to 180º)
float porcentajeInspiratorio = 0.6;         // Percentage of cycle time (inhalation)
float porcentajePausa = 0.0;               // Perfentage of cycle time (pause)

//mechanical variables
float tIns;
float tPausa;
float tEsp;
int pos0=0; 
float velocidadIns = 0.0; 
float velocidadEsp = 0.0; 

//encoder variables
byte DialPos;
byte Last_DialPos;



// Calculation of mechanical variables depending on medical parameters
// A cycle consists of Inhalation, Pause and Exhalation and then repeated; there is NO pause between Exhalation and Inhalation

void calcularConstantes()
{
  float tCiclo = 60 / rpm;                    // Cycle time (seconds)
  tIns = tCiclo * porcentajeInspiratorio;    // Seconds of inhalation
  tPausa = tCiclo * porcentajePausa;        // Seconds of pause
  tEsp = tCiclo - (tIns + tPausa);         // Seconds of exhalation

  velocidadIns = (stepsPerRevolution*microsteps*recorrido / 2) / tIns;   // Inhalation speed in steps per second
  velocidadEsp = (stepsPerRevolution*microsteps*recorrido / 2) / tEsp;  // Exhalation speed in steps per second





// Manual adjustment of the lowest point of the cycle; if possible, replace with a limit switch sensor
void calibrar()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ajustar posicion ");
  lcd.setCursor(0,1);
  lcd.print("inicial ");
  lcd.setCursor(0,3);
  lcd.print("Click para empezar");
  stepper.setMaxSpeed(1000);
  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) pos0 = pos0+0.5*microsteps;
    if (DialPos == 3 && Last_DialPos == 2)pos0 = pos0-0.5*microsteps;
    else {
        stepper.moveTo(pos0);
        stepper.runToPosition();
      }
    Last_DialPos = DialPos;
    
  }
  while (!digitalRead(SW)){}
  
}


// Home menu to define medical parameters
void settings()
{
  // Defines Breaths Per Minute
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("BPMs Adjust    ");
  lcd.print(rpm);
  lcd.setCursor(0,3);
  lcd.print("Click to continue");

// Reading of Encoder
  while (digitalRead(SW)){                                       // Until clicked, i.e. SW = 0
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);         // Binary operation where both inputs are placed together (00, 01, 10 or 11)
    if (DialPos == 3 && Last_DialPos == 1) rpm++;              // If reading gives 00, it means no movement; if reading is 11, it means one step of movement
    if (DialPos == 3 && Last_DialPos == 2) rpm--;              // Depending on whether 11 has been reached from 10 or from 01, direction will be showed
    else {                                                   // This process requires fast cycles while the encoder moves
        lcd.setCursor(16,0);                                // This is why LCD is only updated in case of no movement
        if(rpm>=0 && rpm<10)lcd.print(" ");
        lcd.print(rpm);
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}                            // This avoids that several menus are skipped with just one click



 // Repeated to define Inhalation percentage
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Set percentage");
  lcd.setCursor(0,1);
  lcd.print("inspired     ");
  lcd.print((int)(porcentajeInspiratorio*100));
  lcd.print("%");
  lcd.setCursor(0,3);
  lcd.print("Click to continue");

  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) porcentajeInspiratorio = porcentajeInspiratorio+0.05;
    if (DialPos == 3 && Last_DialPos == 2) porcentajeInspiratorio = porcentajeInspiratorio-0.05;
    else {
        lcd.setCursor(14,1);
        if((int)(porcentajeInspiratorio*100)>=0 && (int)(porcentajeInspiratorio*100)<10)lcd.print(" ");
        lcd.print((int)(porcentajeInspiratorio*100));
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}



  // Repeated to define Pause percentage
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Set percentage");
  lcd.setCursor(0,1);
  lcd.print("pause        ");
  lcd.print((int)(porcentajePausa*100));
  lcd.print("%");
  lcd.setCursor(0,3);
  lcd.print("Click to continue");

  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) porcentajePausa = porcentajePausa+0.05;
    if (DialPos == 3 && Last_DialPos == 2) porcentajePausa = porcentajePausa-0.05;
    else {
        lcd.setCursor(14,1);
        if((int)(porcentajePausa*100)>=0 && (int)(porcentajePausa*100)<10)lcd.print(" ");
        lcd.print((int)(porcentajePausa*100));
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}



 // Repeated to define distance travelled, please replace with volume if equivalence is known
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Range Adjust    ");
  lcd.setCursor(14,1);
  lcd.print((int)(recorrido*100));
  lcd.print("%");
  lcd.setCursor(0,3);
  lcd.print("Click to continue");

  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) recorrido = recorrido+0.05;
    if (DialPos == 3 && Last_DialPos == 2) recorrido = recorrido-0.05;
    else {
        lcd.setCursor(14,1);
        if((int)(recorrido*100)>=0 && (int)(recorrido*100)<10)lcd.print(" ");
        lcd.print((int)(recorrido*100)); 
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}


}


void setup() {

  stepper.setAcceleration(10000);
  lcd.init();
  lcd.backlight();
  
  pinMode(DT, INPUT);   
  pinMode(CLK, INPUT); 
  pinMode(SW, INPUT); 
  digitalWrite(SW, HIGH); 

  pinMode (valveEspPin, OUTPUT);
  pinMode (valveInsPin, OUTPUT);


 // double beep to indicate machine is turned on
  pinMode (buzzerPin, OUTPUT);
  digitalWrite(buzzerPin, HIGH);
  delay (200);
  digitalWrite(buzzerPin, LOW);
  delay (200);
  digitalWrite(buzzerPin, HIGH);
  delay (200);
  digitalWrite(buzzerPin, LOW);
  delay (200);
  
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Wellcome");
  delay (2000);


 // Menus and calculations
  settings();
  calibrar();
  calcularConstantes();  

// Information on LCD during the cycle runtime
  lcd.clear();
  lcd.setCursor(13,0);
  lcd.print(rpm);
  lcd.print(" BPM");
  lcd.setCursor(1,2);
  lcd.print((int)(porcentajeInspiratorio*100));
  lcd.print("% Ins    ");
  lcd.print((int)(recorrido*100));
  lcd.print("% Vol");
}



void loop() { 


    lcd.setCursor(0, 0);
    lcd.print("Expiration");

    digitalWrite(valveEspPin, LOW);       // Exhalation valve opens
//  digitalWrite(valveInsPin, HIGH);     // Inhalation valve closes; please indicate if an SPDT relay is used
    stepper.setMaxSpeed(velocidadEsp);
    stepper.moveTo(pos0 - (stepsPerRevolution*microsteps*recorrido/2));
    stepper.runToPosition();
    delay(10);


    lcd.setCursor(0, 0);
    lcd.print("Inspiration"); 
    
    digitalWrite(valveEspPin, HIGH);    // Exhalation valve closes
//  digitalWrite(valveInsPin, LOW);   // Inhalation valve opens; please indicate if an SPDT relay is used
    stepper.setMaxSpeed(velocidadIns);
    stepper.moveTo(pos0);
    stepper.runToPosition();
    delay(10);

    lcd.setCursor(0, 0);
    lcd.print("Pause       ");
    delay(tPausa*1000);         // Pause time
   
   
}