Punkt	 Qty	 Ref	    Description	 Notes
1	     1	     LCA1207    Arduino Nano	
2	     1       IT31ENB    Schalung KY-040	
3	     1	     LCAL012    LCD 20x4 mit I2C-Modul	
4	     1	     ZBPZS02    Buzzer	
5	     1	     MPP003     NEMA 23	
6	     1	     TB6600     Triebfahrzeugführer Schritt für Schritt	
7	     1	     MPX5010DP  MPX5010DP Differenzdruck MPXDP	
8	     2  	 MEELF12    Elektrosolenoidventil (12 NC)	
9    	 2       DK1A-3V-F  und Bobiva 3V	
10	     4	     1N4007     Diodo 1N4007	
11	     1	     SA5C150W24 Quelle: 24 V 5A	
12	     1	     NMB2147853 NMB 2147853 Ventilator	
13	     1	     LM2576     DC-DC 3A Regelspannung	
14	     1       CJIT005    Notdichtung	
15	     1	     CTBA14P8   IEC-C14 mit Sicherungen	
16	     1	     FUCR5F632, schnelle Sicherung von Glas 6,3A 250V	
17	     1	     CX20020	Cable Schuko	
18	     21	                Cables Dupont Hemba weiblich	
19	     6	                Kabel AWG 20/0,5 mm²	
20	     6                  Elektrodrähte	
