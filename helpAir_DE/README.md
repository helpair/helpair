# helpAir

HelpAir ist eine kostengünstige Intrusionsanlage für das Einatmen von Mengen.

Sie können den Beginn der Operation aus einem einzigen Menü konfigurieren:

- Gezeitenvolumen (% Streckenanteil)
- Atemfrequenz
- der nicht spirituöse Anteil
- Lieferung von Kaffeepausen

Das Projekt wurde spontan von einer Gruppe von Mitarbeitern von Ferrovial und Sngular
entwickelt, die von http://respiradores4all.com unterstützt wurde.

IC3 hat die industrielle Version dieses Dokuments entwickelt.

## Versionen
Derzeit gibt es 3 Versionen des Geschmacksmusters:

- **Fassung 1.1**: Wartung, Motor NMA23, Arduino-Motor und Atemschutzbeutel.
Diese Version ist die Version, die umgesetzt wurde. Durch Inspiration können
500 ml TPidalvolumen erreicht werden. Es ist nicht möglich, das Volumen an
den abgelaufenen Luftdurchsatz anzupassen oder die Systemkonfiguration in
Echtzeit zu ändern.

- **Fassung 1.2**: Kolbenmechanismus, NEAM23XL-Motor mit Schraube, Arduino
und Amb. Diese Version wurde entworfen, aber nicht umgesetzt. Es muss
möglich sein, 1 000 ml des TRidal-Volumens unvoreingenommen zu
überschreiten. Es ist nicht möglich, das Volumen an den abgelaufenen
Luftdurchsatz anzupassen oder die Systemkonfiguration in Echtzeit zu ändern.

- **Fassung 2.0**: Kolbenmechanismus, NEAM23XL-Motor mit Schraube, PLC von
Siemens und Amb. Diese Version wurde entworfen, aber nicht umgesetzt. Es
sollte möglich sein, 1 000 ml des Volumens Tidal durch Inspiration zu
überschreiten, das volumengesteuerte Volumen an den Luftdurchsatz
anzupassen und die Systemkonfiguration in Echtzeit zu ändern.

## Staat
Sie können die Versionen als logischen Fahrplan zu einem Endgerät ansehen. Je nach
Bedarf kann der Fahrplan geändert werden.

Ihre Entwicklung durch das derzeitige Team wurde aufgrund des Stands der Lieferkette
für Komponenten, der Entwicklung der Pandemie in Spanien und der Fortschritte bei
anderen Prototypen nicht fortgesetzt.

Sie kann ein interessanter Ausgangspunkt für die Entwicklung neuer Prototypen an
Orten sein, an denen die Pandemie weniger weit fortgeschritten ist.

## Warnung
**Ein Atemschutzgerät ist ein Medizinprodukt, von dem das Leben einer Person
abhängt, eine Funktionsstörung kann zu schweren Schäden oder sogar zum Tod
führen.**

**Die veröffentlichte Dokumentation dieses Projekts ermöglicht die Entwicklung
eines Forschungsprototyps zum Atmen für die Forschung.**

**In keinem Fall sollte der Prototyp von HelpAir als funktionelle medizinische
Ausrüstung in Betracht gezogen werden und sollte nicht bei Menschen verwendet
werden.**

**Derzeit wurde der Prototyp nicht von der spanischen Arzneimittel-Agentur
validiert.**

## Dokumentation (v.1.1, v.1.2 und v.2.0)
- Präsentation des Projekts (v.1.1)
- Helpf Industrielles Geschmacksmuster (v.2.0)

## Elektronisch (v.1.1)
- BOM Material Bill of Materials
- Prinzipschema

## Code (v1.1)
- Code umgesetzt am Arduino-Kontrolleur

## 3d Geschmacksmuster (v.1.1, v.1.2 und v.2.0)
- Version 1.1 des SEA-Archivs
- Version 1.2 des SEA-Archivs
- Version 2.0 des SEA-Archivs

## Bilder (v.1.1, v.1.2 und v.2.0)
- Bilder Version 1.1
- Bilder Version 1.2
- Bilder Version 2.0

## Projektteam
- Adrián de la Iglesia Valls
- Jorge Vallejo Fuentes
- Alejandro Pérez Ayo
- Francisco Iglesias Sánchez
- Ignacio flags Daviel
- Mariano de Diego
- David López Astorga
- Cristina Cruz Aparicio
- Javier Lázaro Gaspar
- Dr. Eduardo Calle
- Dr. José Antonio de Paz

## Lizenz
- Helpair ist ein Projekt, das im Rahmen der GNU-GLPv3-Lizenz entwickelt wurde.