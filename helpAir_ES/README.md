# helpAir

helpAir es un proyecto de respirador invasivo por control de volumen de bajo coste.

Permite configurar al inicio de la operación desde un único menú:
- el volumen tidal (% de recorrido)
- la frecuencia de respiración
- la proporción inspiración-espiración
- la proporcion de pausa

El proyecto se ha desarrollado de manera espontánea por un grupo de empleados de
Ferrovial y Sngular con el apoyo de http://respiradores4all.com

CI3 ha diseñado la versión industrial del mismo.

## Versiones
Actualmente existen 3 versiones del diseño:

- **Versión 1.1**: mecanismo de leva, motor NEMA23, Arduino y bolsa respiratoria.
Esta versión es la que ha sido implementada probada. Permite alcanzar los
500ml de volumen tidal en vía inspiratoria. No permite ajustar el volumen
impulsado en función del caudal de aire espirado ni modificar la configuración
del sistema en tiempo real.

- **Versión 1.2**: mecanismo de pistón, motor NEMA23XL con husillo, Arduino y
Ambú. Esta versión ha sido diseñada pero no implementada. Debe permitir
superar los 1000ml de volumen tidal en vía inspiratoria. No permite ajustar el
volumen impulsado en función del caudal de aire espirado ni modificar la
configuración del sistema en tiempo real.

- **Versión 2.0**: mecanismo de pistón, motor NEMA23XL con husillo, PLC de
Siemens y Ambú. Esta versión ha sido diseñada pero no implementada. Debe
permitir superar los 1000ml de volumen tidal en vía inspiratoria, ajustar el
volumen impulsado en función del caudal de aire espirado y modificar la
configuración del sistema en tiempo real.

## Estado
Se pueden ver las versiones como un roadmap lógico para lograr un dispositivo final.
Dependiendo de las necesidades a cubrir el roadmap puede ser alterado.

Su desarrollo por el equipo actual ha sido descontinuado debido a la situación de la
cadena de suministro de componentes, la evolución de la pandemia en España y el
grado de avance de otros prototipos.

Puede ser un punto de partida interesante para el desarrollo de nuevos prototipos en
lugares en los que la pandemia esté menos avanzada.

## Advertencia

**Un respirador es un equipo médico del que depende la vida de una persona, un
mal funcionamiento puede resultar en graves daños o incluso la muerte.**

**La documentación publicada de este proyecto permite el desarrollo de un
prototipo de respirador para investigación.**

**En ningún caso debe considerarse el prototipo helpAir como un equipo médico
funcional, y no debe emplearse con humanos.**

**Actualmente el prototipo NO ha sido validado por la Agencia Española del
Medicamento.**

## Documentación (v.1.1, v.1.2 y v.2.0)
- Presentación del proyecto (v.1.1, v.1.2 y v.2.0)
- Diseño helpAir industrial (v.2.0)

## Electrónica (v.1.1 y v.1.2)
- BOM Bill of Materials
- Esquema conexiones

## Código (v1.1)
- Código implementado en el controlador Arduino

## Diseños 3D (v.1.1, v.1.2 y v.2.0)
- Versión 1.1
- Versión 1.2
- Versión 2.0

## Imágenes (v.1.1)

## Equipo del proyecto
- Adrián de la Iglesia Valls
- Jorge Vallejo Fuentes
- Alejandro Pérez Ayo
- Francisco Iglesias Sánchez
- Ignacio Losas Dávila
- Mariano de Diego
- David López Astorga
- Cristina Cruz Aparicio
- Javier Lázaro Gaspar
- Dr. Eduardo Calle
- Dr. José Antonio de Paz

## Licencia
helpAir es un proyecto desarrollado bajo licencia GNU GLPv3

 
 