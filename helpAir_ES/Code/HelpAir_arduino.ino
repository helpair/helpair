/*
 * //////////////////////////////////////  HELPAIR  //////////////////////////////////////
 * Codigo para 1 Arduino Nano
 * Motor Nema 23
 * Driver TB6600
 * 2 electrovalvulas
 * LCD por I2C 20x4
 * Encoder con boton
 * y un Zumbador
 * 
 * Desarrollado por:
 * Adrián de la Iglesia Valls y Francisco Iglesias Sanchez
 * 
 * Para más informacion ver: https://www.respiradores4all.com/prototipo/helpair
 * GNU GENERAL PUBLIC LICENSE V3
*/



#include <AccelStepper.h> //Librería para el motor con driver
#include <Wire.h> // Librería de comunicacion para el I2C
#include <LiquidCrystal_I2C.h> // Librería para LCD


const int stepsPerRevolution = 200; //Pasos del motor en una vuelta
const int microsteps = 32; // Configuracion de micropasos en el driver


// Pines y configuracion del driver para el Motor
#define dirPin 9
#define stepPin 10
#define motorInterfaceType 1


//Pines del encoder con botón 
#define DT 3
#define CLK 2
#define SW 4 //click del botón


#define buzzerPin 12     // Pin para zumbador
#define valveEspPin 7   // Pin para electrovalvula de espiracion
#define valveInsPin 7  // Pin para electrovalvula de inspiracion



AccelStepper stepper = AccelStepper(motorInterfaceType, stepPin, dirPin);  // Definicion del motor con libreria
LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 20, 4);                   // Definicion del LCD con libreria


//Parametros médicos y sus valores por defecto
int rpm = 15;                                  // Respiraciones por minutos
float recorrido = 0.50;                       // Porcentaje de recorrido (1 son 180º)
float porcentajeInspiratorio = 0.6;          // Porcentaje de tiempo del ciclo en ibnspiracion
float porcentajePausa = 0.0;                // Porcentaje de tiempo del ciclo en pausa

//variables mecanicas
float tIns;
float tPausa;
float tEsp;
int pos0=0; 
float velocidadIns = 0.0; 
float velocidadEsp = 0.0; 

//variables para el encoder
byte DialPos;
byte Last_DialPos;



 // Calculo de las variables mecanicas segun los parametros medicos
// Un ciclo consta de Inspiracion, Pausa y Espiracion y se repite, NO hay pausa entre espiracion e inspiracion
void calcularConstantes()
{
  float tCiclo = 60 / rpm;                    // Tiempo de ciclo en segundos
  tIns = tCiclo * porcentajeInspiratorio;    // Segundos inpirando
  tPausa = tCiclo * porcentajePausa;        // Segundos de pausa
  tEsp = tCiclo - (tIns + tPausa);         // Segundos espirando

  velocidadIns = (stepsPerRevolution*microsteps*recorrido / 2) / tIns;   // Velocidad de inspiracion en pasos por segundo
  velocidadEsp = (stepsPerRevolution*microsteps*recorrido / 2) / tEsp;  // Velocidad de espiracion en pasos por segundo
}




// Ajuste manual del punto más bajo del ciclo, sustituir por un final de carrera si es posible
void calibrar()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ajustar posicion ");
  lcd.setCursor(0,1);
  lcd.print("inicial ");
  lcd.setCursor(0,3);
  lcd.print("Click para empezar");
  stepper.setMaxSpeed(1000);
  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) pos0 = pos0+0.5*microsteps;
    if (DialPos == 3 && Last_DialPos == 2)pos0 = pos0-0.5*microsteps;
    else {
        stepper.moveTo(pos0);
        stepper.runToPosition();
      }
    Last_DialPos = DialPos;
    
  }
  while (!digitalRead(SW)){}
  
}


// Menu inicial para definir parametros medicos
void settings()
{
  // Se definen Respiraciones Por Minuto
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ajustar RPMs    ");
  lcd.print(rpm);
  lcd.setCursor(0,3);
  lcd.print("Click para siguiente");

  // Lectura del Encoder
  while (digitalRead(SW)){                                        // Hasta que se haga click, es decir SW = 0
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);         // Operacion binaria donde se ponen ambas entradas juntas (00, 01, 10 o 11)
    if (DialPos == 3 && Last_DialPos == 1) rpm++;               // Si la lectura es 00 no se ha movido nada, si la lectura es 11 se ha movido un paso
    if (DialPos == 3 && Last_DialPos == 2) rpm--;              // Dependiendo de si hemos llegado a 11 desde 10 o desde 01 nos da la direccion
    else {                                                    // Este proceso requiere ciclos muy rapidos mientras se mueve el encoder
        lcd.setCursor(16,0);                                 // Por ello solo se actualiza el LCD cuando no hay movimiento
        if(rpm>=0 && rpm<10)lcd.print(" ");
        lcd.print(rpm);
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}                            // Evita que se salten varios menus en un solo click



  // Se repite para definir Porcentaje de Inspiracion 
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ajustar porcentaje");
  lcd.setCursor(0,1);
  lcd.print("inspirado     ");
  lcd.print((int)(porcentajeInspiratorio*100));
  lcd.print("%");
  lcd.setCursor(0,3);
  lcd.print("Click para siguiente");

  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) porcentajeInspiratorio = porcentajeInspiratorio+0.05;
    if (DialPos == 3 && Last_DialPos == 2) porcentajeInspiratorio = porcentajeInspiratorio-0.05;
    else {
        lcd.setCursor(14,1);
        if((int)(porcentajeInspiratorio*100)>=0 && (int)(porcentajeInspiratorio*100)<10)lcd.print(" ");
        lcd.print((int)(porcentajeInspiratorio*100));
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}



  // Se repite para definir Porcentaje de pausa 
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ajustar porcentaje");
  lcd.setCursor(0,1);
  lcd.print("pausado        ");
  lcd.print((int)(porcentajePausa*100));
  lcd.print("%");
  lcd.setCursor(0,3);
  lcd.print("Click para siguiente");

  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) porcentajePausa = porcentajePausa+0.05;
    if (DialPos == 3 && Last_DialPos == 2) porcentajePausa = porcentajePausa-0.05;
    else {
        lcd.setCursor(14,1);
        if((int)(porcentajePausa*100)>=0 && (int)(porcentajePausa*100)<10)lcd.print(" ");
        lcd.print((int)(porcentajePausa*100));
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}



  // Se repite para definir recorrido, sustituir por volumen si se conoce la equivalencia
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ajustar recorrido    ");
  lcd.setCursor(14,1);
  lcd.print((int)(recorrido*100));
  lcd.print("%");
  lcd.setCursor(0,3);
  lcd.print("Click para siguiente");

  while (digitalRead(SW)){
    DialPos = (digitalRead(CLK) << 1) | digitalRead(DT);
    if (DialPos == 3 && Last_DialPos == 1) recorrido = recorrido+0.05;
    if (DialPos == 3 && Last_DialPos == 2) recorrido = recorrido-0.05;
    else {
        lcd.setCursor(14,1);
        if((int)(recorrido*100)>=0 && (int)(recorrido*100)<10)lcd.print(" ");
        lcd.print((int)(recorrido*100)); 
      }
    Last_DialPos = DialPos;    
  }
  while (!digitalRead(SW)){}


}


void setup() {

  stepper.setAcceleration(10000);
  lcd.init();
  lcd.backlight();
  
  pinMode(DT, INPUT);   
  pinMode(CLK, INPUT); 
  pinMode(SW, INPUT); 
  digitalWrite(SW, HIGH); 

  pinMode (valveEspPin, OUTPUT);
  pinMode (valveInsPin, OUTPUT);


  //doble pitido para marcar el encendido de la maquina
  pinMode (buzzerPin, OUTPUT);
  digitalWrite(buzzerPin, HIGH);
  delay (200);
  digitalWrite(buzzerPin, LOW);
  delay (200);
  digitalWrite(buzzerPin, HIGH);
  delay (200);
  digitalWrite(buzzerPin, LOW);
  delay (200);
  
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Bienvenido");
  delay (2000);


  // Menus y calculos
  settings();
  calibrar();
  calcularConstantes();  

  // Inoformacion en el LCD durante la ejecucion del ciclo
  lcd.clear();
  lcd.setCursor(13,0);
  lcd.print(rpm);
  lcd.print(" RPM");
  lcd.setCursor(1,2);
  lcd.print((int)(porcentajeInspiratorio*100));
  lcd.print("% Ins    ");
  lcd.print((int)(recorrido*100));
  lcd.print("% Vol");
}



void loop() { 


    lcd.setCursor(0, 0);
    lcd.print("Espiracion");

    digitalWrite(valveEspPin, LOW);       // Se abre la valvula de espiracion
//  digitalWrite(valveInsPin, HIGH);     // Se cierra la de inspiracion, comentar si se usa un relé SPDT
    stepper.setMaxSpeed(velocidadEsp);
    stepper.moveTo(pos0 - (stepsPerRevolution*microsteps*recorrido/2));
    stepper.runToPosition();
    delay(10);


    lcd.setCursor(0, 0);
    lcd.print("Inspiracion"); 
    
    digitalWrite(valveEspPin, HIGH);    // Se cierra la valvula de espiracion
//  digitalWrite(valveInsPin, LOW);    // Se abre la de inspiracion, comentar si se usa un relé SPDT
    stepper.setMaxSpeed(velocidadIns);
    stepper.moveTo(pos0);
    stepper.runToPosition();
    delay(10);

    lcd.setCursor(0, 0);
    lcd.print("Pausa       ");
    delay(tPausa*1000);          // Tiempo de Pausa
   
   
}