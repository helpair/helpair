Pozycja	Liczba	Ref	   Opis	Zauważa
1	1	LCA1207	   Arduino Nano	
2	1	IT31ENB	   DZ.U. KY-040	
3	1	LCAL012	   Ekran LCD 20x4 z modułem I2C	
4	1	ZBPZS02	   Buzzier	
5	1	MMPP003	   Nema 23	
6	1	TB6600	   Układ TB6600 4A Engine krok po kroku	
7	1	MPX5010DP  z różnicą ciśnień MPX5010DP	
8	2	IMELF12	   EZawór elektromagnetyczny 12V NC	
9	2	DK1A-3V-F  Cewka przekaźnika półprzewodnikowego 3V	
10	4	1N4007	   Dioda 1N4007	
11	1	SA5C150W24 Źródło 150W 24V 5A	
12	1	NMB2147853 wentylator NMB 2147853	
13	1	LM2576	   DC-DC 3A prąd stały	
14	1	CJIT005	   wyłącznik awaryjny
15	1	CTBA14P8   IEC-C14 z bezpiecznikami
16	1	FUCR5F632  szybki bezpiecznik szkła 6,3A 250V	
17	1	CX20020	   Schuko Cable
18	21		       Kabel Dupont Hemba	
19	6		       Kabel AWG 20 / 0.5mm2	
20	6		       Elektryczny kabel zasilający	
