# helpAir

help Air to tanie urządzenie do inwazyjnego oddychania o niskiej cenie.

Umożliwia skonfigurowanie uruchomienia operacji z jednego menu:
- Wielkość pływów (% w drodze)
- częstotliwość oddychania
- część nieulegająca cięciom
- dostawa przerw na kawę

Projekt został opracowany spontanicznie przez grupę pracowników Ferrovial i Sosolar ze wsparciem http://respiradores4all.com

IC3 opracował wersję przemysłową tego dokumentu.

## Wersje
Obecnie istnieje 3 wersji projektu:

- **Wersja 1.1**:utrzymanie, uruchomienie systemu NMA23, silnik Ardubinowy i mechanizm z workiem oddechowego.Wersja ta jest wersją wprowadzoną w życie.Umożliwia się, aby objętość 500 ml TPdal została uzyskana za pomocą inspiracji.Nie jest możliwe dostosowanie objętości w zależności od wygasłego natężenia przepływu powietrza lub zmiany konfiguracji systemu w czasie rzeczywistym.

- **Wersja 1.2**:mechanizm tłokowy, silnik NEM23XL z śrubą, Arduino i Ampú.Ta wersja została opracowana, ale nie została wdrożona.Musi być możliwe przekroczenie objętości kolby na objętość powyżej 1 000 ml w sposób wolny od uprzedzeń.Nie jest możliwe dostosowanie objętości w zależności od wygasłego natężenia przepływu powietrza lub zmiany konfiguracji systemu w czasie rzeczywistym.

- **Wersja 2.0**:mechanizm tłokowy, silnik NEAM23XL z śrubą, PLC of Siemens i Ambú.Ta wersja została opracowana, ale nie została wdrożona.Powinna ona umożliwić przekroczenie objętości 1 000 ml w drodze inspiracji, dostosować objętość napędzaną wolumenem dostosowaną do natężenia przepływu powietrza i zmienić konfigurację systemu w czasie rzeczywistym.

## Państwo
Można je zobaczyć jako logiczny plan działania na rzecz ostatecznego wyrobu.W zależności od potrzeb w zakresie realizacji planu działania mogą zostać zmienione.

Ich rozwój przez obecny zespół nie był kontynuowany ze względu na obecną sytuację w łańcuchu dostaw komponentów, ewolucję pandemii w Hiszpanii oraz stopień zaawansowania prac nad innymi prototypami.

Może to być interesujący punkt wyjścia do opracowania nowych prototypów w miejscach, w których pandemia jest mniej zaawansowana.

## Ostrzeżenie
**Respirator jest wyrobem medycznym, od którego zależy życie danej osoby, może powodować poważne uszkodzenie lub nawet śmierć.**

**Opublikowana dokumentacja tego projektu pozwala na opracowanie prototypu badawczego, który będzie służył badaniom naukowym.**

**W żadnym wypadku nie należy brać pod uwagę funkcjonalnego działania prototypowego wyposażenia medycznego jako funkcjonalnego sprzętu medycznego i nie powinien on być stosowany u ludzi.**

**Prototyp nie został zatwierdzony przez hiszpańską agencję leków.**

## Dokumentacja (v.1.1, v.1.2 i v.2.0)
- Prezentacja projektu (v.1.1)
- Pomoc w projektowaniu przemysłowym (v.2.0)

## Elektroniczny (v.1.1)
- BOM Bill of Materials
- Schemat ideowy

## Kod (v1.1)
- Kod wdrożony przez administratora danych Arduino

## 3d wzory (v.1.1, v.1.2 i v.2.0)
- Wersja archiwum leasingu podatkowego SLP (1.1)
- Wersja archiwum leasingu podatkowego SLP (1.2)
- Wersja archiwum leasingu podatkowego SLP (2.0)

## Obrazy (v.1.1, v.1.2 i v.2.0)
- Wersja obrazu 1.1
- Wersja obrazu 1.2
- Wersja obrazu 2.0

## Zespół projektu
- Adrán de la Iglesia Valls
- Jorge Vallejo Fuentes
- Alejandro Pérez Ayo
- Francisco Iglesias Sánchez
- Ignacio flagi Daviel
- Mariano de Diego
- David López Astorga
- Cristina Cruz Aparicio
- Javier Lázaro Gaspar
- Dr Eduardo Calle
- José Antonio de Paz

## Licencja
helpdesk jest projektem opracowanym w ramach licencji GNU GLv3
