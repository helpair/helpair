Item	Kol	Ref	  	Opis	Napomena
1	1	LCA1207	   Arduino Nano	
2	1	IT31ENB	   Uređaj za kodiranje KY-040	
3	1	LCAL012	   Ekran LCD 20x4 s modulom I2C	
4	1	ZBPZS02	   Zujalica 	
5	1	MMPP003	   Nema 23	
6	1	TB6600	   Driver TB6600 4A Motor korak po korak	
7	1	MPX5010DP  Senzor  za pritisak diferencijal MPX5010DP	
8	2	IMELF12	   Solenoidni ventil 12V NC	
9	2	DK1A-3V-F  Relej u krutom stanju svitak 3V	
10	4	1N4007	   Dioda 1N4007	
11	1	SA5C150W24  Izvor  napajanja  24V 5A	
12	1	NMB2147853  Ventilator NMB 2147853	
13	1	LM2576	   Modul DC-DC 3A Voltaža Regulator	
14	1	CJIT005	   Set za hitne  slučajeve  	
15	1	CTBA14P8   IEC-C14 s osiguračima	
16	1	FUCR5F632  Stakleni osigurač 6,3A 250V	
17	1	CX20020	   Kabel Schuko	
18	21		       Kabel Dupont ženska spojnica	
19	6		       Kabel AWG 20 / 0.5mm2	
20	6		       Električni kablovi
