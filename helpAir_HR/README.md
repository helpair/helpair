# helpAir 

helpAir je projekt jeftinog invazivnog respiratora koji kontrolira volumen.

Može ga se podesiti na početku operacije s jedinstvenog izbornika: 
- respiracijski volumen  (%  prijeđenog puta)
- frekvencija disanja 
- omjer udisaja-izdisaja 
- omjer  pauza 

Grupa zaposlenika Ferroviala y Sngulara je, uz podršku http://respiradores4all.com, razvila ovaj projekt na spontani način. 

CI3 je dizajnirao njegovu industrijsku verziju. 

## Verzije
U ovom trenutku postoje 3 verzije dizajna:
- **Versión 1.1**: mehanizam bregaste osovine, motor NEMA23, Arduino i vreća za disanje. Ta je verzija izvedena i isprobana. Omogućava dostizanje 500 ml respiracijskog volumena pri udisanju. Ne dozvoljava da se prilagodi volumen koji je poguran u odnosu na izbačeni zrak niti da se promijeni konfiguracija sistema u stvarnom vremenu. 

- **Versión 1.2**: klipni mehanizam, motor NEMA23XL s vijkom, Arduino i Ambú. Taj je verzija dizajnirana, ali nije izvedena. Treba dozvoliti da se prijeđe 1000 ml respiracijskog volumena pri udisanju.Ne dozvoljava da se prilagodi volumen koji je poguran u odnosu na izbačeni zrak niti da se promijeni konfiguracija sistema u stvarnom vremenu.

- **Versión 2.0**: klipni mehanizam, motor NEMA23XL s vijkom i Siemensov  i Ambúov PLC. Taj je verzija dizajnirana, ali nije izvedena. Treba dozvoliti da se prijeđe 1000 ml respiracijskog volumena pri udisanju, prilagoditi volumen koji je poguran u odnosu na izbačeni zrak i promijeniti konfiguraciju sistema u stvarnom vremenu.


## Stanje 
Verzije mogu poslužiti kao logički planovi za postizanje konačnog aparata. Zavisno o potrebama mogu se mijenjati i planovi. 

Sadašnja ekipa nije mogla raditi na prototipu na kontinuirani način zbog problema s nabavom, razvoja pandemije u Španjolskoj i razvojnog stupnja drugih prototipa.  
Može poslužiti kao interesantna polazna točka za razvoj novih prototipa na mjestima gdje pandemija nije toliko napredovala.

## Upozorenje 

**Respirator je medicinska oprema od kojeg zavisi život osobe i njegov loš rad može proizvesti velike štete ili čak i smrt.**

**Dokumentacija objavljena o ovom projektu omogućava razvoj prototipa respiratora za istraživanje.**

**Prototip helpAir se ne može smatrati ni u kojem slučaju funkcionalnim medicinskim aparatom i ne smije se koristiti na ljudima.** 

**U ovom trenutku Španjolska agencija za lijekove NIJE potvrdila valjanost prototipa.** 

## Documentacija (v.1.1, v.1.2 y v.2.0)
- Presentacija projekta  (v.1.1)
- Dizajn industrijskog helpAir-a (v.2.0)

## Elektronika (v.1.1)
- BOM Bill of Materials
- Shema spajanja 

## Kod (v1.1)
- Kod na kontroli Arduino 

## Dizajn 3D (v.1.1, v.1.2 y v.2.0)
- Fajl STL verzija 1.1
- Fajl STL verzija 1.2
- Fajl STL verzija 2.0

## Slike  (v.1.1, v.1.2 y v.2.0)
- Slike verzija 1.1
- Slike verzija 1.2
- Slike verzija 2.0

## Projektna ekipa 
- Adrián de la Iglesia Valls
- Jorge Vallejo Fuentes
- Alejandro Pérez Ayo
- Francisco Iglesias Sánchez
- Ignacio Losas Dávila
- Mariano de Diego
- David López Astorga
- Cristina Cruz Aparicio
- Javier Lázaro Gaspar
- Dr. Eduardo Calle
- Dr. José Antonio de Paz

## Dozvola 
helpAir je projekt koji se razvio prema licenci GNU GLPv3
