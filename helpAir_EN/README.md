# helpAir 

helpAir is a low-cost invasive ventilator project that is  “based on volume controlled ventilation”.

From one single menu, the following parameters can be adjusted at the start upof the device:

- Tidal Volume [This is usually the volume of gas moved in and out of the lungs in any one respiratory cycle]
- Respiratory rate 
- Proportion of the Respiratory Cycle in Expiration / Inspiration
- Pause proportion

helpAir is a spontaneous initiative developed by a group of staff from Ferrovial and Sngular with the support of https://respiradores4all.com/.

CI3 has upgraded the original design into an industrial version, with a view to facilitating mass production.

## Versions
There are 3 currently different versions of this design:

- **Version 1.1**: Contains a cam mechanism, a NEMA23 engine, an Arduino and an ambu bag. This version has been tested. It achieves a tidal volume of up to 500ml. The system does not allow the volume of pushed air to be adjusted according to the flow of expired air. Also, settings cannot be adjusted in real time.

- **Version 1.2**: Contains a valve mechanism, a NEMA23XL engine with a spindle, an Arduino and an ambu bag. This version has been designed but has not yet been implemented. It should allow achievement of a tidal volume greater than 1000ml . The system does not allow the volume of pushed air to be adjusted according to the flow of expired air. Also, settings cannot be adjusted in real time.

- **Version 2.0**: Contains a valve mechanism, a NEMA23XL engine with a spindle, a PLC by Siemens and an ambu bag. This version has past the design state , but has not yet been implemented in a functioning model. It should allow over 1000ml of Tidal volume in inspiration. The volume of pushed (inspired) air can be adjusted according to the flow of expired air. Settings can be adjusted in real time.

## Status
These different versions conform to a logical roadmap to achieve the final device. Depending on the needs, this roadmap may be altered. 

Its development by the current team has been stopped due to the situation of the supply chain of its parts, the current progression of the pandemic in Spain and the development of other prototypes. 

However, it can be a good starting point to develop new prototypes in places where the pandemic is less established.

## Warning
**A ventilator is a medical device which is critical for the life of the patient who needs it. Please note that its malfunction can lead to severe harm or even death.**

**The published documentation on this project allows the development of a ventilator for further clinical testing 
helpAir is a prototype and it should never be considered a functional medical device. It MUST NOT be used with humans.**

**Currently this prototype has NOT been validated by the Spanish Agency of Medicines and Medical Devices.**

## Documents (v.1.1, v.1.2 and v.2.0)
- Project presentation (v.1.1)
- helpAir industrial design (v.2.0)

## Electronics (v.1.1)
- BOM Bill of Materials
- Connection diagram

## Code (v1.1)
- Code implemented onto the Arduino controller

## 3D Designs (v.1.1, v.1.2 y v.2.0)
- STL files for version 1.1
- STL files for version 1.2
- STL files for version 2.0

## Images (v.1.1, v.1.2 y v.2.0)
- Images for version 1.1
- Images for version 1.2
- Images for version 2.0

## Project team
- Adrián de la Iglesia Valls
- Jorge Vallejo Fuentes
- Alejandro Pérez Ayo
- Francisco Iglesias Sánchez
- Ignacio Losas Dávila
- Mariano de Diego
- David López Astorga
- Cristina Cruz Aparicio
- Javier Lázaro Gaspar
- Dr. Eduardo Calle
